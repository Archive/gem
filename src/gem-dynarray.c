#include "gem-dynarray.h"

/* The idea behind this module is to have a data structure that you
   can use as an array, without the disadvantages thereof. This means:

   - Fast item insert & delete.
   - Fast way to find the item at a given position.
   - Fast way to find the position of a given item.
   - Fast way to get the next item & previous item of a given item.
   - Can set per-item widths if so desired.

   This will be useful for finding which measures and staffs need
   redrawing to completely redraw a given rectangle (e.g. redraw of
   the canvas).
*/
/* Implemented using GList for now, until it becomes a speed problem,
   whereupon we use the measured tree. */

struct _GEMDynarray {
  GEMDNode *root;
  GEMGetDNodeFunc dngetfunc;
  GEMSetDNodeFunc dnsetfunc;
  GEMGetWidthFunc dgetwidthfunc;
  gpointer user_data;
};

#if 0
struct _GEMDNode {
  GEMDNode *up;
  gpointer left, right;
  guint width;
  guint8 is_leaf : 1, is_root : 1;
};

static GEMDNode *
gem_dnode_new(GEMDNode *parent)
{
  GEMDNode *retval;

  retval = g_new0(GEMDNode, 1);

  retval->up = parent;
  if(!parent)
    retval->is_root = TRUE;

  return retval;
}

static void
gem_dnode_free(GEMDNode *node)
{
  if(!node->is_leaf) {
    if(node->left) {
      gem_dnode_free(node->left);
      if(node->right)
	gem_dnode_free(node->right);
    }
  }

  g_free(node);
}
#else

struct _GEMDNode {
  GList list;
};
#endif

GEMDynarray *gem_dynarray_new(GEMGetDNodeFunc dngetfunc,
			      GEMSetDNodeFunc dnsetfunc,
			      GEMGetWidthFunc dgetwidthfunc,
			      gpointer user_data)
{
  GEMDynarray *retval;

  retval = g_new0(GEMDynarray, 1);

#if 0
  retval->root = gem_dnode_new(NULL);
#endif
  retval->dngetfunc = dngetfunc;
  retval->dnsetfunc = dnsetfunc;
  retval->dgetwidthfunc = dgetwidthfunc;
  retval->user_data = user_data;

  return retval;
}

void
gem_dynarray_free(GEMDynarray *da)
{
  g_list_free((GList *)da->root);
  g_free(da);
}

gpointer
gem_dynarray_get(GEMDynarray *da, guint position, GEMPositionSpec pspec)
{
#if 0
  guint cur_base_x;
  GEMDNode *cur;
  GEMDNode *ntmp;

  g_return_val_if_fail(da, NULL);
  
  for(cur = da->root, cur_base_x = 0; (ctr < nth); cur = ntmp) {

    if(cur->is_leaf) {
      if(nth == ctr)
	return cur->left;
      else if(nth == (ctr + 1))
	return cur->right;
      else
	g_error("Diff between ctr(%d) and nth(%d) in leaf!", ctr, nth);
    }

    ntmp = cur->left;
    if((nth >= ctr) && (nth <= (ctr + ntmp->nitems))) continue;
    
    ctr += ntmp->nitems;
    ntmp = cur->right;
    g_assert((nth >= ctr) && (nth <= (ctr + ntmp->nitems)));
  }

  return NULL;
#else

  GList *ltmp;
  gpointer prev_item;
  guint so_far;
  guint increment;

  g_return_val_if_fail(da, NULL);

  if(!da->dgetwidthfunc)
    increment = 1;
  for(ltmp = (GList *)da->root, so_far = 0;
      ltmp; prev_item = ltmp->data, ltmp = g_list_next(ltmp)) {
    guint increment;

    if(da->dgetwidthfunc)
      increment = da->dgetwidthfunc(ltmp->data, da->user_data);

    so_far += increment;

    switch(pspec) {
    case POS_CLOSEST_BELOW:
      if(so_far > position) return (ltmp->prev)?(ltmp->prev->data):NULL;
      break;
    case POS_EQUAL:
      if(so_far == position) return ltmp->data;
      break;
    case POS_CLOSEST_ABOVE:
      if(so_far >= position) return ltmp->data;
      break;
    }
  }
  if(pspec == POS_CLOSEST_BELOW)
    return prev_item;
  else
    return NULL;

#endif
}

void
gem_dynarray_insert(GEMDynarray *da, guint position, gpointer data)
{
  gpointer after;

  after = gem_dynarray_get(da, position, POS_EQUAL);

  if(after)
    gem_dynarray_insert_dnode(da, after, data);
}

void
gem_dynarray_remove(GEMDynarray *da, guint position)
{
  gpointer data;

  data = gem_dynarray_get(da, position, POS_EQUAL);

  if(data)
    gem_dynarray_remove_dnode(da, data);
}

void
gem_dynarray_insert_dnode(GEMDynarray *da, gpointer after, gpointer data)
{
  GList *item, *newitem;

  item = (GList *)da->dngetfunc(after, da->user_data);
  g_assert(item);

  newitem = g_list_alloc();
  newitem->prev = item;
  newitem->next = item->next;
  newitem->data = data;
  da->dnsetfunc(data, (GEMDNode *)newitem, da->user_data);
  newitem->next->prev = newitem;
  item->next = newitem;
}

void
gem_dynarray_remove_dnode(GEMDynarray *da, gpointer data)
{
  GList *olditem, *previtem;

  olditem = (GList *)da->dngetfunc(data, da->user_data);
  g_assert(olditem);

  previtem = olditem->prev;
  previtem->next = olditem->next;
  olditem->next->prev = previtem;

  g_list_free_1(olditem);
}

gpointer
gem_dynarray_next(GEMDynarray *da, gpointer data)
{
  return g_list_next(da->dngetfunc(data, da->user_data))->data;
}

gpointer
gem_dynarray_prev(GEMDynarray *da, gpointer data)
{
  return g_list_previous(da->dngetfunc(data, da->user_data))->data;
}

gint
gem_dynarray_position(GEMDynarray *da, gpointer data)
{
  guint counter = 0;
  GList *ltmp;
  guint increment = 1;

  for(ltmp = (GList *)da->root; ltmp && ltmp->data != data; ltmp = g_list_next(ltmp)) {
    if(da->dgetwidthfunc)
      increment = da->dgetwidthfunc(ltmp->data, da->user_data);
    counter += increment;
  }

  if(!ltmp && data) return -1;
  else return counter;
}
