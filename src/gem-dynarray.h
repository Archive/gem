#ifndef GEM_DYNARRAY_H
#define GEM_DYNARRAY_H 1

#include <glib.h>

typedef struct _GEMDynarray GEMDynarray;
typedef struct _GEMDNode GEMDNode;
typedef GEMDNode *(* GEMGetDNodeFunc)(gpointer datum, gpointer user_data);
typedef guint (* GEMGetWidthFunc)(gpointer datum, gpointer user_data);
typedef void (* GEMSetDNodeFunc)(gpointer datum, GEMDNode *dnode,
				 gpointer user_data);
typedef enum { POS_CLOSEST_BELOW, POS_EQUAL, POS_CLOSEST_ABOVE } GEMPositionSpec;

GEMDynarray *gem_dynarray_new(GEMGetDNodeFunc dngetfunc,
			      GEMSetDNodeFunc dnsetfunc,
			      GEMGetWidthFunc dgetwidthfunc,
			      gpointer user_data);
void gem_dynarray_free(GEMDynarray *da);
gpointer gem_dynarray_get(GEMDynarray *da, guint position,
			  GEMPositionSpec pspec);

void gem_dynarray_insert(GEMDynarray *da, guint position, gpointer data);
void gem_dynarray_remove(GEMDynarray *da, guint position);

void gem_dynarray_insert_dnode(GEMDynarray *da, gpointer after,
			       gpointer data);
void gem_dynarray_remove_dnode(GEMDynarray *da, gpointer data);

gpointer gem_dynarray_next(GEMDynarray *da, gpointer data);
gpointer gem_dynarray_prev(GEMDynarray *da, gpointer data);
#define gem_dynarray_width(da) gem_dynarray_position(da, NULL)
gint gem_dynarray_position(GEMDynarray *da,  /* returns -1 on error */
			   gpointer data);

#if 0
void gem_dynarray_update_width(GEMDynarray *da, gpointer data, guint new_width);
#else
/* NOP with current implementation */
#define gem_dynarray_update_width(da, data, new_width)
#endif

#endif
