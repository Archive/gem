#include <gnome.h>

#include "gem-staff-item.h"
#include "gem-note-item.h"

typedef struct {
  GtkWidget *appwin;
  GtkWidget *canvas;
  GnomeCanvasItem *piece;
  GtkAdjustment *zoomadj;
} GEMApp;

static GEMApp *gem_app_new(void);
static gboolean gem_app_handle_delete_event(GtkWidget *widget,
					    GdkEvent *event,
					    GEMApp *app);
static void gem_app_zoom(GtkAdjustment *adjustment, GEMApp *app);


int main(int argc, char *argv[])
{
  GEMApp *app;

  gnome_init("gem", "0.0", argc, argv);

  app = gem_app_new();

  gtk_widget_show_all(app->appwin);

  gtk_main();

  return 0;
}

static GEMApp *
gem_app_new(void)
{
  GEMApp *retval;
  GtkWidget *wtmp;
  GnomeCanvasGroup *root;
  GnomeCanvasItem *piece, *p2;
  GtkAdjustment *hadj, *vadj;

  retval = g_new0(GEMApp, 1);

  retval->appwin = gnome_app_new("gem", "GNOME Electronic Musician");
  retval->zoomadj = GTK_ADJUSTMENT(gtk_adjustment_new(5, 1, 50, 1, 0, 0));

  retval->canvas = gnome_canvas_new();

  hadj = gtk_layout_get_hadjustment(GTK_LAYOUT(retval->canvas));
  vadj = gtk_layout_get_vadjustment(GTK_LAYOUT(retval->canvas));

  root = gnome_canvas_root(GNOME_CANVAS(retval->canvas));
  gnome_canvas_set_scroll_region(GNOME_CANVAS(retval->canvas),
				 -10, -10, 500, 200);
  gnome_canvas_scroll_to(GNOME_CANVAS(retval->canvas), 12, 5);

  piece = retval->piece = gnome_canvas_item_new(root,
						gem_staff_item_get_type(),
						NULL);
  gnome_canvas_item_show(piece);

  p2 = gnome_canvas_item_new(root, gem_note_item_get_type(), "stem_direction", GEM_NOTE_STEM_DOWN, NULL);
  gnome_canvas_item_move(p2, 10, 0);
  gnome_canvas_item_show(p2);

  wtmp = gtk_scrolled_window_new(hadj, vadj);
  gnome_app_set_contents(GNOME_APP(retval->appwin), wtmp);
  gtk_container_add(GTK_CONTAINER(wtmp), retval->canvas);

  gtk_widget_set_usize(wtmp, 800, 600);

  gtk_signal_connect(GTK_OBJECT(retval->appwin), "delete_event",
		     GTK_SIGNAL_FUNC(gem_app_handle_delete_event),
		     retval);
  gtk_signal_connect(GTK_OBJECT(retval->zoomadj), "value_changed",
		     GTK_SIGNAL_FUNC(gem_app_zoom),
		     retval);
  gtk_adjustment_set_value(retval->zoomadj, 40.0);

  wtmp = gtk_spin_button_new(retval->zoomadj, 1, 3);
  gtk_widget_show(wtmp);
  gnome_app_add_docked(GNOME_APP(retval->appwin),
		       wtmp, "zoomer",
		       GNOME_DOCK_ITEM_BEH_NEVER_FLOATING, GNOME_DOCK_TOP, 0, 0, 0);

  return retval;
}

static gboolean
gem_app_handle_delete_event(GtkWidget *widget,
			    GdkEvent *event,
			    GEMApp *app)
{
  gtk_main_quit();
  return TRUE;
}

static void
gem_app_zoom(GtkAdjustment *adjustment, GEMApp *app)
{
  gnome_canvas_set_pixels_per_unit(GNOME_CANVAS(app->canvas), adjustment->value);
}
