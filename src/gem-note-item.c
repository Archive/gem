#include "gem-config.h"
#include "gem-note-item.h"

#include <math.h>

#define NOTE_HEIGHT 1.0
#define NOTE_WIDTH (NOTE_HEIGHT*1.5)
#define STEM_LENGTH -3.5

struct _GEMNoteItem {
  GnomeCanvasItem item;

  GdkGC *gc;
  gdouble duration;
  GEMStemDirection stemdir;
};

struct _GEMNoteItemClass {
  GnomeCanvasItemClass klass;
};

static void gem_note_item_class_init (GEMNoteItemClass *class);
static void gem_note_item_init       (GEMNoteItem      *item);
static void gem_note_item_destroy    (GtkObject            *object);
static void gem_note_item_set_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);
static void gem_note_item_get_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);

static void   gem_note_item_realize(GnomeCanvasItem *item);
static void   gem_note_item_unrealize(GnomeCanvasItem *item);
static void   gem_note_item_update(GnomeCanvasItem *item,
				     double *affine,
				     ArtSVP *clip_path, int flags);

static void   gem_note_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
				   int x, int y, int width, int height);

static double gem_note_item_point (GnomeCanvasItem *item, double x, double y,
				     int cx, int cy,
				     GnomeCanvasItem **actual_item);

static void   gem_note_item_bounds(GnomeCanvasItem *item,
				   double *x1, double *y1,
				   double *x2, double *y2);

static void   gem_note_item_bounds_world(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);

static void   gem_note_item_bounds_pixel(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);

#if defined(CANVAS_BOUNDS_PIXELS)
#define RECALC_BOUNDS gem_note_item_bounds_pixel
#elif defined(CANVAS_BOUNDS_WORLD)
#define RECALC_BOUNDS gem_note_item_bounds_world
#else
#error "What bounding method?"
#endif

static GnomeCanvasItemClass *parent_class;

GtkType
gem_note_item_get_type(void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "GEMNoteItem",
      sizeof (GEMNoteItem),
      sizeof (GEMNoteItemClass),
      (GtkClassInitFunc) gem_note_item_class_init,
      (GtkObjectInitFunc) gem_note_item_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (gnome_canvas_item_get_type (), &item_info);
  }

  return item_type;
}

enum { ARG_0, ARG_DURATION, ARG_STEM_DIR };

static void
gem_note_item_class_init (GEMNoteItemClass *class)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass *) class;
  item_class = (GnomeCanvasItemClass *) class;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  object_class->destroy = gem_note_item_destroy;
  object_class->set_arg = gem_note_item_set_arg;
  object_class->get_arg = gem_note_item_get_arg;
  
  item_class->realize = gem_note_item_realize;
  item_class->unrealize = gem_note_item_unrealize;
  item_class->draw = gem_note_item_draw;
  item_class->point = gem_note_item_point;
  item_class->bounds = gem_note_item_bounds;
  item_class->update = gem_note_item_update;

  gtk_object_add_arg_type("GEMNoteItem::duration", GTK_TYPE_UINT, GTK_ARG_READWRITE, ARG_DURATION);
  gtk_object_add_arg_type("GEMNoteItem::stem_direction", GTK_TYPE_ENUM, GTK_ARG_READWRITE, ARG_STEM_DIR);
}

static void
gem_note_item_init (GEMNoteItem      *item)
{
  GnomeCanvasItem *citem;
  item->duration = 0.25; /* quarter note */
  item->stemdir = GEM_NOTE_STEM_UP;
  citem = GNOME_CANVAS_ITEM(item);
}

static void
gem_note_item_destroy(GtkObject            *object)
{
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(object);
  
  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void
gem_note_item_set_arg(GtkObject            *object,
		      GtkArg               *arg,
		      guint                 arg_id)
{
  GEMNoteItem *nitem;

  nitem = GEM_NOTE_ITEM (object);

  switch(arg_id) {
  case ARG_DURATION:
    nitem->duration = GTK_VALUE_DOUBLE(*arg);
    break;
  case ARG_STEM_DIR:
    nitem->stemdir = GTK_VALUE_ENUM(*arg);
    break;
  default:
    if(GTK_OBJECT_CLASS(parent_class)->set_arg)
      (* GTK_OBJECT_CLASS(parent_class)->set_arg) (object, arg, arg_id);
    break;
  }
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(object));
}

static void gem_note_item_get_arg(GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id)
{
  GEMNoteItem *nitem;

  nitem = GEM_NOTE_ITEM (object);

  switch(arg_id) {
  case ARG_DURATION:
    GTK_VALUE_DOUBLE(*arg) = nitem->duration;
    break;
  case ARG_STEM_DIR:
    GTK_VALUE_ENUM(*arg) = nitem->stemdir;
    break;
  default:
    if(GTK_OBJECT_CLASS(parent_class)->get_arg)
      (* GTK_OBJECT_CLASS(parent_class)->get_arg) (object, arg, arg_id);
    break;
  }
}

static void
gem_note_item_realize(GnomeCanvasItem *item)
{
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(item);

  if(parent_class->realize)
    (* parent_class->realize) (item);

  gitem->gc = gdk_gc_new(GTK_LAYOUT(item->canvas)->bin_window);
}

static void
gem_note_item_unrealize(GnomeCanvasItem *item)
{
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(item);

  gdk_gc_unref(gitem->gc);

  if(parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
gem_note_item_update(GnomeCanvasItem *item,
		      double *affine,
		      ArtSVP *clip_path, int flags)
{
  double i2c[6];
  ArtPoint src, dst, dst2;
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(item);

  if(parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);

  RECALC_BOUNDS(item, &item->x1, &item->y1, &item->x2, &item->y2);

  if((flags & GNOME_CANVAS_UPDATE_VISIBILITY)
     || (flags & GNOME_CANVAS_UPDATE_REQUESTED)) {

    gnome_canvas_item_i2c_affine(item, i2c);

    src.x = -NOTE_WIDTH/2;
    src.y = -NOTE_HEIGHT/2;
    art_affine_point(&dst, &src, i2c);
    src.x = NOTE_WIDTH/2;
    src.y = NOTE_HEIGHT/2;
    art_affine_point(&dst2, &src, i2c);
    gnome_canvas_request_redraw(item->canvas, dst.x, dst.y, dst2.x, dst2.y);
  }
}

static gboolean
gem_note_item_is_filled(GEMNoteItem *nitem)
{
  return !(nitem->duration >= 0.5);
}

static gboolean
gem_note_item_has_stem(GEMNoteItem *nitem)
{
  if(nitem->stemdir == GEM_NOTE_STEM_NONE)
    return FALSE;
  else
    return !(nitem->duration >= 1.0);
}

static void
gem_note_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
		   int x, int y, int width, int height)
{
  GEMNoteItem *gitem;
  gdouble i2c[6];
#define SKEW_FACTOR 3.0
  static const ArtPoint src[] = {
    {-NOTE_WIDTH/2.0, 0.0},
    {-NOTE_WIDTH/SKEW_FACTOR, -NOTE_HEIGHT/SKEW_FACTOR},
    {0.0, -NOTE_HEIGHT/2.0},
    {NOTE_WIDTH/SKEW_FACTOR, -NOTE_HEIGHT/SKEW_FACTOR},
    {NOTE_WIDTH/2.0, 0.0},
    {NOTE_WIDTH/SKEW_FACTOR, NOTE_HEIGHT/SKEW_FACTOR},
    {0.0, NOTE_HEIGHT/2.0},
    {-NOTE_WIDTH/SKEW_FACTOR, NOTE_HEIGHT/SKEW_FACTOR}
  };
  ArtPoint dst[sizeof(src)/sizeof(ArtPoint)];
  GdkPoint points[sizeof(src)/sizeof(ArtPoint)];
  int i;

  gitem = GEM_NOTE_ITEM(item);

  gnome_canvas_item_i2c_affine(item, i2c);

  for(i = 0; i < sizeof(src)/sizeof(ArtPoint); i++) {
    art_affine_point(&dst[i], &src[i], i2c);
    points[i].x = dst[i].x - x;
    points[i].y = dst[i].y - y;
  }

  gdk_draw_polygon(drawable, gitem->gc, gem_note_item_is_filled(gitem), points, sizeof(src)/sizeof(ArtPoint));

  if(gem_note_item_has_stem(gitem)) {
    ArtPoint asrc, adst, bsrc, bdst;

    asrc.x = bsrc.x = NOTE_WIDTH/2.0;
    asrc.y = 0.0;
    bsrc.y = STEM_LENGTH * gitem->stemdir;

    art_affine_point(&adst, &asrc, i2c);
    art_affine_point(&bdst, &bsrc, i2c);

    gdk_draw_line(drawable, gitem->gc, adst.x - x - 1, adst.y - y, bdst.x - x - 1, bdst.y - y);
  }
}

static double
gem_note_item_point (GnomeCanvasItem *item, double x, double y,
		       int cx, int cy,
		       GnomeCanvasItem **actual_item)
{
  double xdist, ydist;
  double x1, x2, y1, y2;
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(item);

  *actual_item = item;

  x1 = -NOTE_WIDTH/2;
  y1 = -NOTE_HEIGHT/2;
  x2 = NOTE_WIDTH/2;
  y2 = NOTE_HEIGHT/2;

  if(x < x1)
    xdist = x1 - x;
  else if(x > x2) 
    xdist = x - x2;
  else 
    xdist = 0;

  if(y < y1)
    ydist = y1 - y;
  else if(y > y2)
    ydist = y - y2;
  else 
    ydist = 0;

  if((xdist == 0) && gem_note_item_has_stem(gitem)) {
    /* maybe this point is on the stem */

    if((x == NOTE_WIDTH/2)
       && (y * gitem->stemdir > STEM_LENGTH * gitem->stemdir))
      xdist = 0;
  }

  return hypot(xdist, ydist);
}

static void
gem_note_item_bounds(GnomeCanvasItem *item,
		     double *x1, double *y1,
		     double *x2, double *y2)
{
  GEMNoteItem *gitem;

  gitem = GEM_NOTE_ITEM(item);

  *x1 = -NOTE_WIDTH/2;
  *y1 = -NOTE_HEIGHT/2;
  *x2 = NOTE_WIDTH/2;
  *y2 = NOTE_HEIGHT/2;

  if(gem_note_item_has_stem(gitem)) {
    gdouble stemend_x;

    stemend_x = STEM_LENGTH * gitem->stemdir;
    *y1 = MIN(*y1, stemend_x);
    *y2 = MAX(*y2, stemend_x);
  }
}

static void
gem_note_item_bounds_world(GnomeCanvasItem *item,
			   double *x1, double *y1,
			   double *x2, double *y2)
{
  double i2w[6];
  ArtPoint src1, src2, dst;

  gnome_canvas_item_i2w_affine(item, i2w);
  gem_note_item_bounds(item, &src1.x, &src1.y, &src2.x, &src2.y);

  art_affine_point(&dst, &src1, i2w);
  *x1 = dst.x;
  *y1 = dst.y;

  art_affine_point(&dst, &src2, i2w);
  *x2 = dst.x;
  *y2 = dst.y;
}

static void
gem_note_item_bounds_pixel(GnomeCanvasItem *item,
			    double *x1, double *y1,
			    double *x2, double *y2)
{
  double i2c[6];
  ArtPoint src1, src2, dst;

  gnome_canvas_item_i2c_affine(item, i2c);
  gem_note_item_bounds(item, &src1.x, &src1.y, &src2.x, &src2.y);

  art_affine_point(&dst, &src1, i2c);
  *x1 = dst.x;
  *y1 = dst.y;

  art_affine_point(&dst, &src2, i2c);
  *x2 = dst.x;
  *y2 = dst.y;
}
