#ifndef GEM_CLEF_ITEM_H
#define GEM_CLEF_ITEM_H 1

#include <gnome.h>

#include "gem-data.h"

#define GEM_TYPE_CLEF_ITEM         (gem_clef_item_get_type ())
#define GEM_CLEF_ITEM(obj)        (GTK_CHECK_CAST ((obj), GEM_TYPE_CLEF_ITEM, GEMClefItem))
#define GEM_CLEF_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GEM_TYPE_CLEF_ITEM, GEMClefItemClass))
#define GEM_IS_CLEF_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GEM_TYPE_CLEF_ITEM))
#define GEM_IS_CLEF_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GEM_TYPE_CLEF_ITEM))

typedef struct _GEMClefItem GEMClefItem;
typedef struct _GEMClefItemClass GEMClefItemClass;
GtkType gem_clef_item_get_type(void);

typedef enum {
  GEM_CLEF_NONE = 0,
  GEM_CLEF_TREBLE = 1,
  GEM_CLEF_SOPRANO = 1,
  GEM_CLEF_ALTO = 2,
  GEM_CLEF_TENOR = 3,
  GEM_CLEF_BASS = 4
} GEMClefType;
#endif
