#ifndef GEM_STAFF_ITEM_H
#define GEM_STAFF_ITEM_H 1

#include <gnome.h>

#include "gem-data.h"

#define GEM_TYPE_STAFF_ITEM            (gem_staff_item_get_type ())
#define GEM_STAFF_ITEM(obj)            (GTK_CHECK_CAST ((obj), GEM_TYPE_STAFF_ITEM, GEMStaffItem))
#define GEM_STAFF_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GEM_TYPE_STAFF_ITEM, GEMStaffItemClass))
#define GEM_IS_STAFF_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GEM_TYPE_STAFF_ITEM))
#define GEM_IS_STAFF_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GEM_TYPE_STAFF_ITEM))

typedef struct _GEMStaffItem GEMStaffItem;
typedef struct _GEMStaffItemClass GEMStaffItemClass;
GtkType gem_staff_item_get_type(void);
void gem_staff_item_set_width(GEMStaffItem *item, gdouble width);
gdouble gem_staff_item_get_width(GEMStaffItem *item);

#endif
