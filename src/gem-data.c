#include "gem-data.h"

GEMPiece *
gem_piece_new(void)
{
  GEMPiece *retval;

  retval = g_new0(GEMPiece, 1);

  return retval;
}

void
gem_piece_free(GEMPiece *piece)
{
  g_list_foreach(piece->staffs, (GFunc)gem_staff_free, NULL);
  g_list_free(piece->staffs);

  g_list_foreach(piece->voices, (GFunc)gem_voice_free, NULL);
  g_list_free(piece->voices);

  g_free(piece);
}

void
gem_piece_add_staff(GEMPiece *piece, GEMStaff *staff)
{
  g_return_if_fail(piece);

  piece->staffs = g_list_append(piece->staffs, staff);
}

void
gem_piece_remove_staff(GEMPiece *piece, GEMStaff *staff)
{
  g_return_if_fail(piece);

  piece->staffs = g_list_remove(piece->staffs, staff);
}

void
gem_piece_add_voice(GEMPiece *piece, GEMVoice *voice)
{
  g_return_if_fail(piece);

  piece->voices = g_list_append(piece->voices, voice);
}

void
gem_piece_remove_voice(GEMPiece *piece, GEMVoice *voice)
{
  g_return_if_fail(piece);

  piece->voices = g_list_remove(piece->voices, voice);
}

GEMPosition *
gem_position_new(void)
{
  GEMPosition *retval;

  retval = g_new0(GEMPosition, 1);

  return retval;
}

static void
gem_position_set_up(GEMPosition *position, gpointer up, gpointer user_data)
{
  position->up = up;
}

static gpointer
gem_position_get_up(GEMPosition *position, gpointer user_data)
{
  return position->up;
}

static guint
gem_position_get_width(GEMPosition *position, gpointer user_data)
{
  return 10;
}

void
gem_position_free(GEMPosition *position)
{
  g_free(position->items);
  g_free(position);
}

GEMStaff *
gem_staff_new(void)
{
  GEMStaff *retval;

  retval = g_new0(GEMStaff, 1);

  retval->positions = gem_dynarray_new(gem_position_get_up,
				       gem_position_set_up,
				       gem_position_get_width,
				       NULL);

  return retval;
}

void
gem_staff_free(GEMStaff *staff)
{
  g_free(staff);
}

GEMVoice *
gem_voice_new(GEMStaff *on_staff)
{
  GEMVoice *retval;

  retval = g_new0(GEMVoice, 1);
  retval->staff = on_staff;
  retval->positions = gem_dynarray_new((GEMGetDNodeFunc)gem_position_get_up,
				       (GEMSetDNodeFunc)gem_position_set_up,
				       (GEMGetWidthFunc)gem_position_get_width,
				       NULL);

  return retval;
}

void
gem_voice_free(GEMVoice *voice)
{
  gem_dynarray_free(voice->positions);
}
