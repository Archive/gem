#ifndef GEM_NOTE_ITEM_H
#define GEM_NOTE_ITEM_H 1

#include <gnome.h>

#include "gem-data.h"

#define GEM_TYPE_NOTE_ITEM         (gem_note_item_get_type ())
#define GEM_NOTE_ITEM(obj)        (GTK_CHECK_CAST ((obj), GEM_TYPE_NOTE_ITEM, GEMNoteItem))
#define GEM_NOTE_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GEM_TYPE_NOTE_ITEM, GEMNoteItemClass))
#define GEM_IS_NOTE_ITEM(obj)         (GTK_CHECK_TYPE ((obj), GEM_TYPE_NOTE_ITEM))
#define GEM_IS_NOTE_ITEM_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GEM_TYPE_NOTE_ITEM))

typedef struct _GEMNoteItem GEMNoteItem;
typedef struct _GEMNoteItemClass GEMNoteItemClass;
GtkType gem_note_item_get_type(void);

typedef enum { GEM_NOTE_STEM_DOWN=-1, GEM_NOTE_STEM_NONE=0, GEM_NOTE_STEM_UP=1 } GEMStemDirection;
#endif
