#include "gem-staff-item.h"
#include "gem-config.h"

#include <math.h>

#define STAFF_MIN_WIDTH 100
#define STAFF_LINE_SPACING 1
#define STAFF_HEIGHT (STAFF_LINE_SPACING * 4)
#define STAFF_STARTBOX_WIDTH 1

struct _GEMStaffItem {
  GnomeCanvasItem item;

  GdkGC *gc;

  gdouble width, dwidth;
};

struct _GEMStaffItemClass {
  GnomeCanvasItemClass klass;
};

static void gem_staff_item_class_init (GEMStaffItemClass *class);
static void gem_staff_item_init       (GEMStaffItem      *item);
static void gem_staff_item_destroy    (GtkObject            *object);
static void gem_staff_item_set_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);
static void gem_staff_item_get_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);

static void   gem_staff_item_realize(GnomeCanvasItem *item);
static void   gem_staff_item_unrealize(GnomeCanvasItem *item);
static void   gem_staff_item_update(GnomeCanvasItem *item,
				     double *affine,
				     ArtSVP *clip_path, int flags);

static void   gem_staff_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
				   int x, int y, int width, int height);

static double gem_staff_item_point (GnomeCanvasItem *item, double x, double y,
				     int cx, int cy,
				     GnomeCanvasItem **actual_item);
static void   gem_staff_item_bounds(GnomeCanvasItem *item,
				    double *x1, double *y1,
				    double *x2, double *y2);
static void   gem_staff_item_bounds_world(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);
static void   gem_staff_item_bounds_pixel(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);

#if defined(CANVAS_BOUNDS_PIXELS)
#define RECALC_BOUNDS gem_staff_item_bounds_pixel
#elif defined(CANVAS_BOUNDS_WORLD)
#define RECALC_BOUNDS gem_staff_item_bounds_world
#endif

static GnomeCanvasItemClass *parent_class;

GtkType
gem_staff_item_get_type(void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "GEMStaffItem",
      sizeof (GEMStaffItem),
      sizeof (GEMStaffItemClass),
      (GtkClassInitFunc) gem_staff_item_class_init,
      (GtkObjectInitFunc) gem_staff_item_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (gnome_canvas_item_get_type (), &item_info);
  }

  return item_type;
}

static void
gem_staff_item_class_init (GEMStaffItemClass *class)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	object_class = (GtkObjectClass *) class;
	item_class = (GnomeCanvasItemClass *) class;

	parent_class = gtk_type_class (gnome_canvas_item_get_type ());

	object_class->destroy = gem_staff_item_destroy;
	object_class->set_arg = gem_staff_item_set_arg;
	object_class->get_arg = gem_staff_item_get_arg;

	item_class->realize = gem_staff_item_realize;
	item_class->unrealize = gem_staff_item_unrealize;
	item_class->draw = gem_staff_item_draw;
	item_class->point = gem_staff_item_point;
	item_class->bounds = gem_staff_item_bounds;
	item_class->update = gem_staff_item_update;
}

static void
gem_staff_item_init (GEMStaffItem      *item)
{
  GnomeCanvasItem *citem;
  item->width = STAFF_MIN_WIDTH;
  citem = GNOME_CANVAS_ITEM(item);
}

static void
gem_staff_item_destroy(GtkObject            *object)
{
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(object);
  
  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void
gem_staff_item_set_arg(GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
}

static void gem_staff_item_get_arg(GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id)
{
}

static void
gem_staff_item_realize(GnomeCanvasItem *item)
{
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  if(parent_class->realize)
    (* parent_class->realize) (item);

  gitem->gc = gdk_gc_new(GTK_LAYOUT(item->canvas)->bin_window);
}

static void
gem_staff_item_unrealize(GnomeCanvasItem *item)
{
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  gdk_gc_unref(gitem->gc);

  if(parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
gem_staff_item_update(GnomeCanvasItem *item,
		      double *affine,
		      ArtSVP *clip_path, int flags)
{
  int cx1, cy1, cx2, cy2;
  double i2c[6];
  ArtPoint src, dst, dst2;
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  if(parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);

  RECALC_BOUNDS(item, &item->x1, &item->y1, &item->x2, &item->y2);

  if(flags & GNOME_CANVAS_UPDATE_VISIBILITY) {

    gnome_canvas_item_i2c_affine(item, i2c);

    src.x = src.y = 0;
    art_affine_point(&dst, &src, i2c);
    src.x = gitem->width;
    src.y = STAFF_HEIGHT;
    art_affine_point(&dst2, &src, i2c);
    gnome_canvas_request_redraw(item->canvas, dst.x, dst.y, dst2.x, dst2.y);

  } else if(flags & GNOME_CANVAS_UPDATE_REQUESTED) {

    gnome_canvas_item_i2c_affine(item, i2c);
    src.x = gitem->width;
    src.y = 0;
    art_affine_point(&dst, &src, i2c);
    cx1 = dst.x; cy1 = dst.y;
    src.x = gitem->width + gitem->dwidth;
    src.y = STAFF_HEIGHT;
    art_affine_point(&dst, &src, i2c);
    cx2 = dst.x; cy2 = dst.y;
    gnome_canvas_request_redraw(item->canvas, cx1, cy1, cx2, cy2);
  }
}

static void
gem_staff_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
		     int x, int y, int width, int height)
{
  GEMStaffItem *gitem;
  gdouble i2c[6];
  ArtPoint src, dst[4];
  int i;
  GdkPoint points[4];

  gitem = GEM_STAFF_ITEM(item);

  gnome_canvas_item_i2c_affine(item, i2c);

  /* Draw lines */
  for(i = 0; i < 5; i++) {
    src.x = 0;
    src.y = i * STAFF_LINE_SPACING;
    art_affine_point(&dst[0], &src, i2c);
    src.x = gitem->width;
    art_affine_point(&dst[1], &src, i2c);
    gdk_draw_line(drawable, gitem->gc,
		  (gint)(dst[0].x - x), (gint)(dst[0].y - y),
		  (gint)(dst[1].x - x), (gint)(dst[1].y - y));
  }

  src.x = 0;
  src.y = 0;
  art_affine_point(&dst[0], &src, i2c);
  src.x = STAFF_STARTBOX_WIDTH;
  art_affine_point(&dst[1], &src, i2c);
  src.y = STAFF_HEIGHT;
  art_affine_point(&dst[2], &src, i2c);
  src.x = 0;
  art_affine_point(&dst[3], &src, i2c);
  for(i = 0; i < 4; i++) {
    points[i].x = dst[i].x - x;
    points[i].y = dst[i].y - y;
  }
  gdk_draw_polygon(drawable, gitem->gc, TRUE, points, 4);

  src.x = gitem->width;
  src.y = 0;
  art_affine_point(&dst[0], &src, i2c);
  src.y = STAFF_HEIGHT;
  art_affine_point(&dst[1], &src, i2c);
  gdk_draw_line(drawable, gitem->gc,
		(gint)(dst[0].x - x), (gint)(dst[0].y - y),
		(gint)(dst[1].x - x), (gint)(dst[1].y - y));
}

static double
gem_staff_item_point (GnomeCanvasItem *item, double x, double y,
		       int cx, int cy,
		       GnomeCanvasItem **actual_item)
{
  double xdist, ydist;
  double x1, x2, y1, y2;
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  *actual_item = item;

  x1 = 0;
  y1 = 0;
  x2 = x1 + gitem->width;
  y2 = y1 + STAFF_HEIGHT;

  if(x < x1)
    xdist = x1-x;
  else if(x > x2) 
    xdist = x-x2;
  else 
    xdist = 0;

  if(y < y1)
    ydist = y1-y;
  else if(y > y2)
    ydist = y-y2;
  else 
    ydist = 0;

  return hypot(xdist, ydist);
}

static void
gem_staff_item_bounds(GnomeCanvasItem *item,
		      double *x1, double *y1,
		      double *x2, double *y2)
{
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  *x1 = *y1 = 0;
  *x2 = gitem->width; *y2 = STAFF_HEIGHT;
}

static void
gem_staff_item_bounds_dummy(GnomeCanvasItem *item,
			    double *x1, double *y1,
			    double *x2, double *y2)
{
  *x1 = *y1 = -1000;
  *x2 = *y2 = 1000;
}

static void
gem_staff_item_bounds_world(GnomeCanvasItem *item,
			    double *x1, double *y1,
			    double *x2, double *y2)
{
  double i2w[6];
  ArtPoint src, dst;
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  gnome_canvas_item_i2w_affine(item, i2w);

  src.x = src.y = 0;
  art_affine_point(&dst, &src, i2w);
  *x1 = dst.x;
  *y1 = dst.y;

  src.x = gitem->width;
  src.y = STAFF_HEIGHT;
  art_affine_point(&dst, &src, i2w);
  *x2 = dst.x;
  *y2 = dst.y;
}

static void
gem_staff_item_bounds_pixel(GnomeCanvasItem *item,
			    double *x1, double *y1,
			    double *x2, double *y2)
{
  double i2w[6];
  ArtPoint src, dst;
  GEMStaffItem *gitem;

  gitem = GEM_STAFF_ITEM(item);

  gnome_canvas_item_i2c_affine(item, i2w);

  src.x = src.y = 0;
  art_affine_point(&dst, &src, i2w);
  *x1 = dst.x;
  *y1 = dst.y;

  src.x = gitem->width;
  src.y = STAFF_HEIGHT;
  art_affine_point(&dst, &src, i2w);
  *x2 = dst.x;
  *y2 = dst.y;
}

void
gem_staff_item_set_width(GEMStaffItem *item, gdouble width)
{
  item->dwidth = width - item->width;
  item->width = width;
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(item));
}

gdouble
gem_staff_item_get_width(GEMStaffItem *item)
{
  return item->width;
}
