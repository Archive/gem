#ifndef GEM_DATA_H
#define GEM_DATA_H 1

#include <glib.h>

#include "gem-dynarray.h"

/* These data structures are meant to represent the whole
   music piece. (Not yet finished) */

/* If you modify this, you may also need to modify the size of the note_mod
   field in GEMNote */
typedef enum {
  NOTE_MOD_DOUBLE_SHARP = 0,
  NOTE_MOD_SHARP = 1,
  NOTE_MOD_DEFAULT = 2,
  NOTE_MOD_NATURAL = 3,
  NOTE_MOD_FLAT = 4,
  NOTE_MOD_DOUBLE_FLAT = 5
} GEMNoteModifiers;

typedef enum {
  NOTE_FLAG_TIED = 1<<0,
  NOTE_FLAG_BEAMED = 1<<1,
  NOTE_FLAG_STACCATO = 1<<2,
  NOTE_FLAG_STEM_UP = 1<<3,
  NOTE_FLAG_STEM_DOWN = 1<<4,
  NOTE_FLAG_NEXT_STAFF = 1<<5,
  NOTE_FLAG_PREV_STAFF = 1<<6
} GEMNoteFlags;

typedef struct {
  guint8 flags; /* GEMNoteFlags */
  guint8 pitch;
  guint8 mod : 3; /* GEMNoteModifiers */
} GEMNote;

typedef struct {
  guint8 clef_type : 2;
} GEMClef;

typedef struct {
} GEMBarline;

/* These all need to be negative numbers, to allow the optimization of the
   GEMPosition union */
typedef enum {
  ITEM_CLEF,
  ITEM_NOTE,
  ITEM_BARLINE
} GEMItemType;

typedef struct _GEMStaff GEMStaff;

typedef struct {
  GEMItemType type;
  union {
    GEMNote gemnote;
    GEMClef gemclef;
    GEMBarline gembarline;
  } u;
  GEMStaff *staff;
} GEMItem;

/* We need to subclass things into items, modifiers, and decorations. */

typedef struct {
  gpointer up;
  gint nitems;
  GEMItem *items;
} GEMPosition;

GEMPosition *gem_position_new(void);
void gem_position_free(GEMPosition *position);

struct _GEMStaff {
  GEMDynarray *positions;
};

GEMStaff *gem_staff_new(void);
void gem_staff_free(GEMStaff *staff);

typedef struct {
  GEMDynarray *positions;
  GEMStaff *staff;
} GEMVoice;

GEMVoice *gem_voice_new(GEMStaff *on_staff);
void gem_voice_free(GEMVoice *voice);

typedef struct {
  GList *staffs;
  GList *voices;
} GEMPiece;

GEMPiece *gem_piece_new(void);
void gem_piece_free(GEMPiece *piece);
void gem_piece_add_staff(GEMPiece *piece, GEMStaff *staff);
void gem_piece_remove_staff(GEMPiece *piece, GEMStaff *staff);
void gem_piece_add_voice(GEMPiece *piece, GEMVoice *voice);
void gem_piece_remove_voice(GEMPiece *piece, GEMVoice *voice);

/*
  If there is a 1:n relationship, gemdynarray.
  If there is a 1:1 relationship, pointer.
  If there is no given relationship beside a link, parallel in parent.

  Entities: voices, staves, notes/other items, measures, piece.

  Each measure holds multiple items.

  Each item has a measure-position and other basic info.

  Each range has a staff and a list of items.

  Each voice has a list of ranges.

  Each staff has nothing.
*/
#endif
