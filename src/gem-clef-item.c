#include "gem-config.h"
#include "gem-clef-item.h"

#include <math.h>

struct _GEMClefItem {
  GnomeCanvasItem item;

  GdkGC *gc;

  GEMClefType clef_type, old_clef_type;
};

struct _GEMClefItemClass {
  GnomeCanvasItemClass klass;
};

static void gem_clef_item_class_init (GEMClefItemClass *class);
static void gem_clef_item_init       (GEMClefItem      *item);
static void gem_clef_item_destroy    (GtkObject            *object);
static void gem_clef_item_set_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);
static void gem_clef_item_get_arg    (GtkObject            *object,
					GtkArg               *arg,
					guint                 arg_id);

static void   gem_clef_item_realize(GnomeCanvasItem *item);
static void   gem_clef_item_unrealize(GnomeCanvasItem *item);
static void   gem_clef_item_update(GnomeCanvasItem *item,
				     double *affine,
				     ArtSVP *clip_path, int flags);

static void   gem_clef_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
				   int x, int y, int width, int height);

static double gem_clef_item_point (GnomeCanvasItem *item, double x, double y,
				     int cx, int cy,
				     GnomeCanvasItem **actual_item);

static void   gem_clef_item_bounds(GnomeCanvasItem *item,
				   double *x1, double *y1,
				   double *x2, double *y2);

static void   gem_clef_item_bounds_world(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);

static void   gem_clef_item_bounds_pixel(GnomeCanvasItem *item,
					  double *x1, double *y1,
					  double *x2, double *y2);

static void   gem_clef_item_bounds_for(GEMClefType ctype,
				       double *x1, double *y1,
				       double *x2, double *y2);

#if defined(CANVAS_BOUNDS_PIXELS)
#define RECALC_BOUNDS gem_clef_item_bounds_pixel
#elif defined(CANVAS_BOUNDS_WORLD)
#define RECALC_BOUNDS gem_clef_item_bounds_world
#else
#error "What bounding method?"
#endif

static GnomeCanvasItemClass *parent_class;

GtkType
gem_clef_item_get_type(void)
{
  static GtkType item_type = 0;

  if (!item_type) {
    GtkTypeInfo item_info = {
      "GEMClefItem",
      sizeof (GEMClefItem),
      sizeof (GEMClefItemClass),
      (GtkClassInitFunc) gem_clef_item_class_init,
      (GtkObjectInitFunc) gem_clef_item_init,
      NULL, /* reserved_1 */
      NULL, /* reserved_2 */
      (GtkClassInitFunc) NULL
    };

    item_type = gtk_type_unique (gnome_canvas_item_get_type (), &item_info);
  }

  return item_type;
}

enum { ARG_0, ARG_CLEF_TYPE };

static void
gem_clef_item_class_init (GEMClefItemClass *class)
{
  GtkObjectClass *object_class;
  GnomeCanvasItemClass *item_class;
  
  object_class = (GtkObjectClass *) class;
  item_class = (GnomeCanvasItemClass *) class;
  
  parent_class = gtk_type_class (gnome_canvas_item_get_type ());
  
  object_class->destroy = gem_clef_item_destroy;
  object_class->set_arg = gem_clef_item_set_arg;
  object_class->get_arg = gem_clef_item_get_arg;
  
  item_class->realize = gem_clef_item_realize;
  item_class->unrealize = gem_clef_item_unrealize;
  item_class->draw = gem_clef_item_draw;
  item_class->point = gem_clef_item_point;
  item_class->bounds = gem_clef_item_bounds;
  item_class->update = gem_clef_item_update;

  gtk_object_add_arg_type("GEMClefItem::clef_type", GTK_TYPE_ENUM, GTK_ARG_READWRITE, ARG_CLEF_TYPE);
}

static void
gem_clef_item_init (GEMClefItem      *item)
{
  item->clef_type = GEM_CLEF_TREBLE;
}

static void
gem_clef_item_destroy(GtkObject            *object)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(object);
  
  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (* GTK_OBJECT_CLASS(parent_class)->destroy) (object);
}

static void
gem_clef_item_set_arg(GtkObject            *object,
		      GtkArg               *arg,
		      guint                 arg_id)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM (object);

  switch(arg_id) {
  case ARG_CLEF_TYPE:
    gitem->old_clef_type = gitem->clef_type;
    gitem->clef_type = GTK_VALUE_ENUM(*arg);
    break;
  default:
    if(GTK_OBJECT_CLASS(parent_class)->set_arg)
      (* GTK_OBJECT_CLASS(parent_class)->set_arg) (object, arg, arg_id);
    gitem->old_clef_type = GEM_CLEF_NONE;
    break;
  }
  gnome_canvas_item_request_update(GNOME_CANVAS_ITEM(object));
}

static void gem_clef_item_get_arg(GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM (object);

  switch(arg_id) {
  case ARG_CLEF_TYPE:
    GTK_VALUE_ENUM(*arg) = gitem->clef_type;
    break;
  default:
    if(GTK_OBJECT_CLASS(parent_class)->get_arg)
      (* GTK_OBJECT_CLASS(parent_class)->get_arg) (object, arg, arg_id);
    break;
  }
}

static void
gem_clef_item_realize(GnomeCanvasItem *item)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(item);

  if(parent_class->realize)
    (* parent_class->realize) (item);

  gitem->gc = gdk_gc_new(GTK_LAYOUT(item->canvas)->bin_window);
}

static void
gem_clef_item_unrealize(GnomeCanvasItem *item)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(item);

  gdk_gc_unref(gitem->gc);

  if(parent_class->unrealize)
    (* parent_class->unrealize) (item);
}

static void
gem_clef_item_update(GnomeCanvasItem *item,
		      double *affine,
		      ArtSVP *clip_path, int flags)
{
  double i2c[6];
  ArtPoint src, src2, dst, dst2;
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(item);

  if(parent_class->update)
    (* parent_class->update) (item, affine, clip_path, flags);

  RECALC_BOUNDS(item, &item->x1, &item->y1, &item->x2, &item->y2);

  gnome_canvas_item_i2c_affine(item, i2c);

  if((flags & GNOME_CANVAS_UPDATE_REQUESTED)
     && gitem->old_clef_type) {
    gem_clef_item_bounds_for(gitem->old_clef_type, &src.x, &src.y, &src2.x, &src2.y);
    art_affine_point(&dst, &src, i2c);
    art_affine_point(&dst2, &src2, i2c);
    gnome_canvas_request_redraw(item->canvas, dst.x, dst.y, dst2.x, dst2.y);
  }

  if((flags & GNOME_CANVAS_UPDATE_VISIBILITY)
     || (flags & GNOME_CANVAS_UPDATE_REQUESTED)) {

    gem_clef_item_bounds_for(gitem->clef_type, &src.x, &src.y, &src2.x, &src2.y);
    art_affine_point(&dst, &src, i2c);
    art_affine_point(&dst2, &src2, i2c);
    gnome_canvas_request_redraw(item->canvas, dst.x, dst.y, dst2.x, dst2.y);
  }
}

static void
gem_clef_item_draw(GnomeCanvasItem *item, GdkDrawable *drawable,
		   int x, int y, int width, int height)
{
  GEMClefItem *gitem;
  gdouble i2c[6];
#define SKEW_FACTOR 3.0
  static const ArtPoint src_treble[] = {
  };
  static const ArtPoint src_alto[] = {
  };
  static const ArtPoint src_bass[] = {
  };
  gdouble offset = 0.0;
  static const struct { const ArtPoint * const points; guint npoints; } src_list[] = {
    {NULL, 0}, /* GEM_CLEF_NONE */
    {src_treble, sizeof(src_treble)/sizeof(ArtPoint)}, /* GEM_CLEF_TREBLE */
    {src_alto, sizeof(src_alto)/sizeof(ArtPoint)}, /* GEM_CLEF_ALTO */
    {src_alto, sizeof(src_alto)/sizeof(ArtPoint)}, /* GEM_CLEF_TENOR */
    {src_bass, sizeof(src_bass)/sizeof(ArtPoint)} /* GEM_CLEF_BASS */
  };
  ArtPoint *src;
  ArtPoint *dst;
  GdkPoint *points;
  int i, npoints;

  gitem = GEM_CLEF_ITEM(item);

  gnome_canvas_item_i2c_affine(item, i2c);

  src = (ArtPoint *)src_list[gitem->clef_type].points;
  g_return_if_fail(src);
  npoints = src_list[gitem->clef_type].npoints;

  dst = alloca(sizeof(ArtPoint) * npoints);
  points = alloca(sizeof(GdkPoint) * npoints);

  for(i = 0; i < npoints; i++) {
    ArtPoint thispoint;

    thispoint = src[i];
    thispoint.y += offset;

    art_affine_point(&dst[i], &thispoint, i2c);
    points[i].x = dst[i].x - x;
    points[i].y = dst[i].y - y;
  }

  gdk_draw_polygon(drawable, gitem->gc, TRUE, points, npoints);
}

static double
gem_clef_item_point (GnomeCanvasItem *item, double x, double y,
		       int cx, int cy,
		       GnomeCanvasItem **actual_item)
{
  double xdist, ydist;
  double x1, x2, y1, y2;
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(item);

  *actual_item = item;

  gem_clef_item_bounds_for(gitem->clef_type, &x1, &y1, &x2, &y2);

  if(x < x1)
    xdist = x1 - x;
  else if(x > x2) 
    xdist = x - x2;
  else 
    xdist = 0;

  if(y < y1)
    ydist = y1 - y;
  else if(y > y2)
    ydist = y - y2;
  else 
    ydist = 0;

  return hypot(xdist, ydist);
}

static void
gem_clef_item_bounds(GnomeCanvasItem *item,
		     double *x1, double *y1,
		     double *x2, double *y2)
{
  GEMClefItem *gitem;

  gitem = GEM_CLEF_ITEM(item);

  gem_clef_item_bounds_for(gitem->clef_type, x1, y1, x2, y2);
}

static void
gem_clef_item_bounds_world(GnomeCanvasItem *item,
			   double *x1, double *y1,
			   double *x2, double *y2)
{
  double i2w[6];
  ArtPoint src1, src2, dst;

  gnome_canvas_item_i2w_affine(item, i2w);
  gem_clef_item_bounds(item, &src1.x, &src1.y, &src2.x, &src2.y);

  art_affine_point(&dst, &src1, i2w);
  *x1 = dst.x;
  *y1 = dst.y;

  art_affine_point(&dst, &src2, i2w);
  *x2 = dst.x;
  *y2 = dst.y;
}

static void
gem_clef_item_bounds_pixel(GnomeCanvasItem *item,
			    double *x1, double *y1,
			    double *x2, double *y2)
{
  double i2c[6];
  ArtPoint src1, src2, dst;

  gnome_canvas_item_i2c_affine(item, i2c);
  gem_clef_item_bounds(item, &src1.x, &src1.y, &src2.x, &src2.y);

  art_affine_point(&dst, &src1, i2c);
  *x1 = dst.x;
  *y1 = dst.y;

  art_affine_point(&dst, &src2, i2c);
  *x2 = dst.x;
  *y2 = dst.y;
}

static void gem_clef_item_bounds_for(GEMClefType ctype,
				     double *x1, double *y1,
				     double *x2, double *y2)
{
  gdouble offset = 0.0;

  switch(ctype) {
  case GEM_CLEF_TREBLE:
    *x1 = 0.0;
    *y1 = -1.25;
    *x2 = 3.0;
    *y2 = 6.0;
    break;
  case GEM_CLEF_TENOR:
    offset = -1.0;
  case GEM_CLEF_ALTO:
    *x1 = 0.0;
    *y1 = 0.0 + offset;
    *x2 = 3.0;
    *y2 = 4.0 + offset;
    break;
  case GEM_CLEF_BASS:
    *x1 = 0.0;
    *y1 = 0.0;
    *x2 = 3.0;
    *y2 = 4.0;
    break;
  default:
    g_assert_not_reached();
    break;
  }
}
